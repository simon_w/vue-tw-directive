# Vue Tailwind Directive

This is a vue plugin that allows you to group Tailwind (or similar) classes by
their variant using the `v-tw` directive.

## Installation

To use this plugin, you must install it in both your Vue app and your Tailwind
config.

Firstly, add the plugin:

* NPM: `npm i vue-tw-directive`
* Yarn: `yarn add vue-tw-directive`

Secondly, add the plugin to your Vue app:

```js
import twDirective from 'vue-tw-directive';

app.use(twDirective);
```

Finally, add the `transform` helper to your Tailwind config. To do this, you
must use the object bersion of the `content` setting, not the array.

```js
import { transform } from 'vue-tw-directive'

export default {
  content: {
    files: [...],
    transforms: {
      vue: transform,
    },
  },
}
```

## Usage

You can now use the `v-tw` directive in your `.vue` files. Variants are passed
as modifiers to the directive. For example,

```html
<button v-tw.hover="'bg-red-500 text-red-800'">
```

### Current Restrictions

The Tailwind transform is not very smart. It requires double quotes to open the
attribute then single quotes to provide the value (all directives that start
with `v-` are dynamic, like `:class`).
