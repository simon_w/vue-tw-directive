import directive from './src/directive.js';

export { default as transform } from './src/transform.js';

export default {
  install(app) {
    app.directive('tw', directive);
  }
}
