import { mount } from '@vue/test-utils'
import twDirective from './directive';

const HoverComponent = {
  template: '<div v-tw.hover="classes" />',
  props: {
    classes: {
      type: String,
      required: true,
    },
  },
};

const HoverSmComponent = {
  template: '<div v-tw.hover.sm="classes" />',
  props: {
    classes: {
      type: String,
      required: true,
    },
  },
};

const SmHoverComponent = {
  template: '<div v-tw.sm.hover="classes" />',
  props: {
    classes: {
      type: String,
      required: true,
    },
  },
};

it('Sets classes with a single variant', ({ expect }) => {
  const wrapper = mount(HoverComponent, {
    global: {
      directives: {
        tw: twDirective,
      }
    },
    props: {
      classes: 'a b c',
    }
  });

  expect(wrapper.classes())
    .toContain('hover:a')
    .toContain('hover:b')
    .toContain('hover:c')
});

it('Sets classes with two variants', ({ expect }) => {
  const wrapper = mount(HoverSmComponent, {
    global: {
      directives: {
        tw: twDirective,
      }
    },
    props: {
      classes: 'a b c',
    },
  });

  expect(wrapper.classes())
    .toContain('hover:sm:a')
    .toContain('hover:sm:b')
    .toContain('hover:sm:c')
});

it('Sets classes with two variants, maintaining order', ({ expect }) => {
  const wrapper = mount(SmHoverComponent, {
    global: {
      directives: {
        tw: twDirective,
      }
    },
    props: {
      classes: 'a b c',
    },
  });

  expect(wrapper.classes())
    .toContain('sm:hover:a')
    .toContain('sm:hover:b')
    .toContain('sm:hover:c')
});

it('Handles extra characgters', ({ expect }) => {
  const wrapper = mount(HoverComponent, {
    global: {
      directives: {
        tw: twDirective,
      }
    },
    props: {
      classes: 'content-["hello:there"]',
    }
  });

  expect(wrapper.classes()).toContain('hover:content-["hello:there"]')
});

it('Handles changes', async ({ expect }) => {
  const wrapper = mount(HoverComponent, {
    global: {
      directives: {
        tw: twDirective,
      }
    },
    props: {
      classes: 'a b c',
    }
  });

  expect(wrapper.classes())
    .toContain('hover:a')
    .toContain('hover:b')
    .toContain('hover:c')

  await wrapper.setProps({ classes: 'd b f'});

  expect(wrapper.classes())
    .toContain('hover:d')
    .toContain('hover:b')
    .toContain('hover:f')
    .not
    .toContain('hover:a')
    .toContain('hover:c');
});
