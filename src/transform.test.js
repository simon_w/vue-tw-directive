import transform from './transform.js';

it.concurrent.each([
  ['v-tw.sm="\'block\'"', 'sm:block'],
  ['v-tw.sm.hover="\'block\'"', 'sm:hover:block'],
  ['v-tw.hover.sm="\'block\'"', 'hover:sm:block'],
  ['v-tw.sm="\'block inline\'"', 'sm:block sm:inline'],
  ['Extra content around <span v-tw.sm="\'block\'">the transform</span>', 'Extra content around <span sm:block>the transform</span>'],
  ['v-tw.sm="\'block\'" v-tw.hover="\'block\'"', 'sm:block hover:block']
])('transforms `%s` to `%s`', function (input, expected) {
  expect(transform(input)).toBe(expected);
})
