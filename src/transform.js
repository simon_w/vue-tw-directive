export default function twTransform(input) {
  if (typeof input !== 'string') {
    return input;
  }

  let output = '';

  const matcher =
    /\bv-tw\.(?<modifiers>(?:[a-zA-Z0-9-]+\.?)+)\s*=\s*"'(?<classes>[^']+)'"/g;

  let prevIndex = 0;
  let match = matcher.exec(input);

  while (match !== null) {
    output += input.substring(prevIndex, match.index);
    prevIndex = matcher.lastIndex;

    const modifiers = match.groups.modifiers.replaceAll('.', ':');
    const classes = match.groups.classes
      .split(/\s+/g)
      .map((cs) => `${modifiers}:${cs}`);

    output += classes.join(' ');
    match = matcher.exec(input);
  }

  output += input.substring(prevIndex);

  return output;
}
