export default function twDirective(el, binding) {
  const prefix = `${[...Object.keys(binding.modifiers), ''].join(':')}`;

  if (binding.oldValue) {
    if (binding.oldValue === binding.value) {
      return;
    }

    const toRemove = new Set(binding.oldValue.split(/\s+/).filter(Boolean));
    toRemove.forEach((c) => {
      el.classList.remove(`${prefix}${c}`);
    });
  }

  const classes = new Set(binding.value.split(/\s+/).filter(Boolean));
  classes.forEach((c) => {
    el.classList.add(`${prefix}${c}`);
  });
}
